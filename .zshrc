# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd beep extendedglob
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/gopi/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# My Aliases
alias tcmount="truecrypt -t -k ''  --protect-hidden=no /mnt/01CFEF88EA2EA570/encdirs/myenc_container ~/enc"

alias wd="w3m -dump"
#directory hacks :)
alias ..="cd .."
alias .2="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."
alias .5="cd ../../../../.."
alias l="ls"

dvim() {
    dt=`date +"%Y%m%d"`
    vim "${dt}.$1"
}


j(){
    if [[ $1 == "-a" ]]; then
        autojump --add `pwd`
        echo "added: `pwd`"
    else
        cd `autojump $1`
        ls
    fi
}
cl(){
    cd $1;ls
}
#a calculator
c(){
bc << EOF
scale=4
$@
quit
EOF
}

#git aliases
cd_git_root(){
    cd `git rev-parse --show-toplevel`
}
alias Gfc="git init . && git add . && git commit -a -m 'first commit'"
alias Gs="cd_git_root;git status -s"
alias Gd="cd_git_root;git diff --word-diff --color-words"
alias Gdc="cd_git_root;git diff --word-diff --color-words --cached"
alias Gp="cd_git_root;git push"
alias Gpl="cd_git_root;git pull"
alias Gc="cd_git_root;git commit -a"
alias Gl="cd_git_root;git lg"
alias Ga="cd_git_root;git add ."
alias Gl1="cd_git_root;git log --pretty=format:'%C(yellow)%ad%Cred%d %Creset%s%Cblue' --decorate --numstat --date=short"
alias Gl2="cd_git_root;git log --pretty=format:'%C(yellow)%ad%Cred%d %Creset%s%Cblue' --decorate --date=short"


#django aliases
alias Pr="python manage.py runserver"
alias Pm="python manage.py migrate"
alias Ds="django-admin startproject"
alias Da="django-admin startapp"

alias notes="vim ~/notes.txt"
alias vt="vim /txtdb/tododb/16/mar/todo.txt"
alias agt="ag -t"
alias blg.ls="stat -c '%x %n' * | cut -f1,4 -d' ' |sort -r"
alias hg="cat ~/.histfile|ag"
alias x=startx

uncat () {
    ls -l | awk '{print $2  $9}'
}

export PATH="/opt/anaconda2/bin:$PATH:."
export EDITOR=vim

source ~/.config/zsh/zsh-git-prompt/zshrc.sh
PROMPT='%~%b$(git_super_status)
%# '


pb () {
  #curl -F "c=@${1:--}" https://ptpb.pw/
    curl -sF "c=@${1:--}" -w "%{redirect_url}" 'https://ptpb.pw/?r=1' -o /dev/stderr | xsel -l /dev/null -b
}

#our ssh agent settings
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent`
    ssh-add ~/.ssh/id_rsa
fi
