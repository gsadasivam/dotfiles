#! /bin/bash
pushd .
cd $DJPARENT
echo "creating proj: $DJROOT"
django-admin startproject $DJROOT
echo "creating templates dir: $DJPARENT/templates/$DJROOT"
mkdir -p "$DJROOT/templates/$DJROOT/"
echo "Adding template dir to $DJROOT/settings.py"
sed -i "s/'DIRS': \[/'DIRS': [os.path.join(BASE_DIR, \"templates\")/" $DJPARENT/$DJROOT/$DJROOT/settings.py

URLS="$DJROOT/$DJROOT/urls.py"
echo "editing $URLS"
echo "   added import url, include"
sed -i "s/import url/import url, include/" $URLS
echo "   added from $DJROOT import views"
sed -i "s/import admin/import admin\nfrom $DJROOT import views/" $URLS
echo "   added urlpatter for views.home"
sed -i "s/urlpatterns = \[/urlpatterns = [\n    url(r'^$', views.home, name='home'),/" $URLS

echo "creating a simple $DJROOT/$DJROOT/views.py"
cat <<EOF >$DJROOT/$DJROOT/views.py
from django.shortcuts import render

def home(request):
    return render(request, "$DJROOT/index.html")
EOF

echo "creating a simple home page under templates"
cat <<EOF >$DJROOT/templates/$DJROOT/index.html
<html>
<h1>Welcome!</h1>
<b>It's working!</b>
This is our home!
</html>
EOF

cd $DJPARENT/$DJROOT
#python manage.py  makemigrations
#python manage.py migrate

popd
