#! /bin/bash
pushd .
cd "$DJPARENT/$DJROOT"
echo "creating the app: $1"
django-admin startapp "$1"
echo "creating a static folder for css files"
mkdir -p $DJPARENT/$DJROOT/$1/static/css
echo "copying a sample.css to the app"
cp ~/.snips/dj/css_store/sample.css $DJPARENT/$DJROOT/$1/static/css/

echo "creating a template dir for app: templates/$1"
mkdir -p "templates/$1"

echo "adding a sample html welcome page: templates/$1/index.hmtl"
cat <<EOF > "templates/$1/index.html"
<html>
{% load staticfiles %}
<head>
<link rel="stylesheet" href="{% static 'css/sample.css' %}">
</head>
<h1>Welcome to app</h1>
<b>Things are working here too! </b>
</html>
EOF

echo "Installing app in $DJROOT/settings.py"
sed -i "s/INSTALLED_APPS = \[/INSTALLED_APPS = [\n    \'$1\',/" "$DJROOT/settings.py"
sed -i "s/urlpatterns = \[/urlpatterns = [\n    url(r'^$1\/', include('$1.urls', namespace='$1')),/" "$DJROOT/urls.py"

echo "editing $1/urls.py to add a simple $1_home handler"
cat <<EOF >$1/urls.py
from django.conf.urls import url
from $1 import views

urlpatterns = [
    url(r'^$', views.$1_home, name='$1_home'),
]
EOF

echo "editing $1/views.py to implement the handler"

cat <<EOF >$1/views.py
from django.shortcuts import render

def $1_home(request):
    return render(request, '$1/index.html')
EOF

cd $DJPARENT/$DJROOT
python manage.py makemigrations
python manage.py migrate
