class B {
public:
  B() { cout << " B() "; }
  virtual void f() { cout << " B.f() "; }
  virtual ~B() { cout << " ~B() "; }
};

class D : public B {
public:
  D() { cout << " D() "; }
  void f() { cout << " D.f() "; }
  ~D() { cout << " ~D() "; }
};
