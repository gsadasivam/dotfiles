# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export PATH="/opt/anaconda2/bin:$PATH:."

#xrandr --output LVDS --gamma 0.8:0.8:0.8 --brightness 0.8
setxkbmap -option ctrl:nocaps

export EDITOR=vim

alias j=autojump

alias gfc="git init . && git add . && git commit -a -m 'first commit'"

alias notes="vim ~/notes.txt"
alias vt="vim /txtdb/tododb/16/mar/todo.txt"
alias agt="ag -t"
