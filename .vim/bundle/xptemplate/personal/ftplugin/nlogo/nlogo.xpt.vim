XPTemplate priority=personal

XPT setup "initial setup code
to setup
    clear-all
    ask patches [
        set pcolor blue
        if random 100 < 10 [
            set pcolor green
        ]
        `cursor^
    ]
    reset-ticks
end
globals [`globavars^]
patches-own [`patchvars^]
turtles-own [`turtlevars^]

XPT to "to function init
to `button name^
    `cursor^
end

XPT p "ask patches
ask patches [
    `cursor^
]

XPT count "count patches and store in var
set `var^ count neighbors with [pcolor = `color^]

XPT if "if condition
if `(a=3) or (b=4)^ [
    set pcolor `color^
]
