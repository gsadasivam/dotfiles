XPTemplate priority=personal

XPT initclass "A fast class boot up prog to test
#include <iostream>
using namespace std;

class `className^`$BRfun^{
public:
    `className^(`$SParg`ctorParam?`$SParg^);
    ~`className^();
    `className^(`$SParg^const `className^ &cpy`$SParg^);
    `cursor^
private:
};

`className^::`className^(`ctorParam?^)`$BRfun^{
    cout << "`className^ constructor called\n";
}

`className^::~`className^()`$BRfun^{
    cout << "`className^ destructor called.\n";
}

`className^::`className^(`$SParg^const `className^ &cpy`$SParg^)`$BRfun^{
    cout << "`className^ copy constructor called\n";
}

int main(int argc, char *argv[])
{
    `className^ * ptr_`className^ = new `className^();
    delete ptr_`className^;
    return 0;
}
