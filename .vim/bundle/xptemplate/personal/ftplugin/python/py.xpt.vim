XPTemplate priority=personal

"  ==================== Python Basics ==================

XPT coroutine "coroutine using yield, send and close
def coroutine():
    while True:
        val = (yield i) # <<<< cool stuff here!
        print("Received %s" % val)

sequence = coroutine()
sequence.next()
sequence.next()
sequence.send('hello')
sequence.close()

XPT listfiles "list files recursively os.walk
import fnmatch
import os
for root, dirnames, filenames in os.walk(`path^):
    for filename in fnmatch.filter(filenames, '*.`extension^'):
        matches.append(os.path.join(root, filename))

XPT sortdict "sorting dict by value sorted..dict..key=keyfunc
import operator
x = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
sorted_x = sorted(x.items(), key=operator.itemgetter(1))
#sorted(d.items(), key=lambda x: x[1]) #this works too

"  ==================== string related ==================

XPT join
`variable^  = "`delmiter^".join(`string_list^)
`cursor^

XPT i.sio
from cStringIO import StringIO as sio

"  ==================== time math ==================
XPT time.sec "gets current seconds
`seconds^ = time.time()

XPT time.str2sec "date string to time stamp
`seconds_var^ = int(time.mktime(dt.strptime(`your date string^,'%d/%m/%Y %H:%M:%S').timetuple()))

XPT time.sec2str "date time stamp to date string
dt.fromtimestamp(`time stamp^).strftime('%Y-%m-%d %H:%M:%S')

XPT time.strp "strptime
`time_struct^ = dt.strptime(`strvar^,`%Y-%m-%d^) `#delthis %I=12 hr, %M = min, %m= month, %p =am/pm %a=sun %A=SUN $1.weekday() 0->mon! multiline thing^

XPT time.now "dt.now
from datetime import datetime as dt
`timetuple^ = dt.now()


XPT i.timedelta "import datetime esp for timedelta
from datetime import timedelta

XPT time.cs.lastthu "Get the last thursday of current month
now = dt.now()
month = calendar.monthcalendar(now.year, now.month)
#get the max date of last week or previous week
expiry_day = max(month[-1][calendar.THURSDAY], month[-2][calendar.THURSDAY])


"  =========== IO related ===================
XPT read "read file
with open(`filename^, '`r^') as file:
    `file_content^ = file.read()
`4^

XPT p
print `var^
`2^

XPT plist
for item in `listname^:
    print item `2^
XPT grep
`result^ = re.findall(`pattern^, `string_var^)
`4^

XPT re "regex compile search ang group
`pattern^ = re.compile('`pattern^')
$1_matches = $1.search(`text^)
if $1_matches:
    $1_results = $1_matches.group()
`cursor^

XPT i.dt "datetime import
from datetime import datetime as dt

XPT i.c "collection deque, namedtuple counter, orderdict
from collections import ` Counter, deque(), namedtuple(), OrderedDict^
import collections

XPT i.m "import matplotlib pyplot
from matplotlib import pyplot as plt
import seaborn as sns

XPT m.subplot "insert subplots
fig = plt.figure()
ax = fig.add_subplot(2,2,1)
ax.set_title("`title^")
`plt.plot(x,y)^

ax = fig.add_subplot(2,2,2)
ax.set_title("`title^")
`#plt.plot(x,ya repeate two more times^

plt.show()

XPT m.label "Labels rotation and font settings.
xl = ax.get_xticklabels()
for label in xl:
    label.set_fontname( 'Times New Roman')
    label.set_fontsize(14)
    label.set_rotation(45)

XPT m.date "Date format for X axis from matplotlib mdates
import matplotlib.dates as mdates
fig = plt.figure()
ax = fig.add_subplot (111)
months = mdates.MonthLocator()
ax.xaxis.set_major_locator(months)
fmt = mdates.DateFormatter( '%d-%b')
ax.xaxis.set_major_formatter(fmt)
xlim = list(ax.get_xlim())
xlim[0] = mdates.date2num(dt(2012,3,1))
ax.set_xlim(xlim)
fig.autofmt_xdate()

XPT m.pie "cool pie chart using sns
colors = sns.dark_palette("skyblue", `count:8^, reverse=True)
explode = `[0,0,0, 0.2, 0, 0.2]^
`datalist^ = `[12,32,34,3.3,22]^
`labels^ = `label list^
plt.pie(`datalist^, explode=explode, colors=colors, labels=`labels^, autopct= '%2.0f', shadow=True)
plt.show()

XPT m.hist "matplotlib histogram quick basic code
fig = plt.figure(figsize=(12, 14))
fig.suptitle(`title^, fontsize=20)
plt.xticks(`xvallist^, `xlabel_list^)
plt.xlim(`range(0,20,10) or list^)
plt.hist(`obj_list^, bins=`bincount^)
fig.savefig(`savetitle^, bbox_inches="tight")
plt.show()

XPT m.chart "a clean chart without chart junk
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
            (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
            (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
            (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
            (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r / 255., g / 255., b / 255.)

plt.figure(figsize=(12, 14))

# Remove the plot frame lines. They are unnecessary chartjunk.
ax = plt.subplot(111)
ax.spines["top"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["left"].set_visible(False)

ax.get_xaxis().tick_bottom()  #ticks are not needed on top and right
ax.get_yaxis().tick_left()

plt.ylim(`<0,90>^)
plt.xlim(`<0,100>^)  #plt x and y axis limits

for y in range(10, 91, 10):
    plt.plot(range($2), [y] * len(range($2)), "--", lw=0.5, color="black", alpha=0.3)

plt.yticks(`range(0,90,10)^, `label_list^, fontsize=14)
plt.xticks(fontsize=14)

# Remove the tick marks; they are unnecessary with the tick lines we just plotted.
plt.tick_params(axis="both", which="both", bottom="off", top="off",
            labelbottom="on", left="off", right="off", labelleft="on")
plt.`plot or hist or bar^( lw=2.7, color=tableau20[0]) `#del this usetableau20[i] inside loop.^

#title text or use simply plt.title("title",fontsize=22)
plt.text(`x^, `y^, `title string^, fontsize=17, ha="center")

"  ======================= Kungfu Panda =====================

XPT i.p "import pandas
import pandas as pd

XPT i "import headers
import `header^

XPT csv "read a csv file
    rows = csv.reader(open(`file.csv^, 'r'), quotechar="\"")
    for r in rows:
        print r   #process each row as appropriate

XPT p.reindex "pick and choose row, cols for new df
    `filtered_df^ = `df^.reindex(`row_list^,columns = `col_list^, method = `ffill or bfill^)

XPT p.group "group and aggregation mean,sum,std etc.
`grouped^ = `df^.groupby(`YName^)
for `index^, `frame^ in `grouped^
    print `index^, `frame^
`grouped^.agg(['mean', 'sum', 'std', lambda x: x.max() - x.min()])
`grouped^.transform(len) #creates a column after applying transf to subframes
`grouped^.shift(1) #cool group aware shift without blindly shifting down one row

XPT p.csv "pandas read_csv
pd.read_csv(`path\file.csv^, header=None, names=`col name list^, `index_col='x label'^)

XPT p.tocsv "export DateFrame to csv
`df^.to_csv(`csv_file^, float_format='%.2f')

XPT p.cs.addrow "append a row to current DataFrame
s = {'date':cur_date, 'start_bal':start_bal, 'pnl':pnl, 'brokerage': 0, 'final_bal':(pnl+start_bal)}
pdf2 = pd.DataFrame([s])
df = df.append(pdf2)

XPT p.df.list
df = pd.DataFrame({
    `colname_str^ : `col_list^,
`    `more...`
{{^    `colname_str^ : `col_list^,
`    `more...`
^`}}^})

XPT p.df.i "random int of 5X3 matrix
df = pd.DataFrame(np.random.randint(0,2,(5,3)), columns = ['a', 'b', 'c'])

XPT p.df.np "DataFrame from np 6x5 matrix
df = pd.DataFrame(np.arange(30).reshape(6, 5), columns=list('abcde'))

XPT p.s "panda Series creation
s = pd.Series(`col_list^, index=`indx_list^)

XPT p.if "conditional masking assignment of values
df.ix[`cond df.A > 3^, `target col 'A' or col list ['B','C']^] = `assig_val^

XPT p.strp "parse date
dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S')
d['TradeTime'] = d['TradeTime'].apply(dateparse)

XPT p.pivot "pivoting stack unstack melt
#date person score stream now pivots into persons and their scores per day
#3 col format (indx, vars, vals) to table of variables
df.pivot(index=`date^, columns=`person^, values=`scores^)
df.stack() #stacked multi index table usually after pivoting
df.unstack() #unstacks

#case: first_name, lastname, height, weight table into
#stream of 3 col fomat (index, vars, value)
df.melt(id_vars = ['key1', 'key2']) #same as unstack except it works on multi index

"  ======================= Numpy snippets =====================

XPT i.n "import numpy
import numpy as np

XPT i.n.l "import lineaer algos from numpy
import numpy.linalg as la

XPT n.r
np.random.randint(`a^, `b^, `dimension eg:(3,3)^)
np.random.randn(5) #a simple 5 random num

XPT n.l
`var^ = np.linspace(`start^,`stop^, `step^)
`cursor^

"  ======================= Scipy snippets =====================

XPT i.s "import scipy stats
import scipy.stats as stats

"  ======================= Tkinter snippets =====================

XPT  tk.main "bare TkInter code
from Tkinter import *

class MyFirstGUI():
    def __init__(self):
        self.root = Tk()
        self.root.title('`Title^')
        self.root.mainloop()

if __name__ == '__main__':
    app = MyFirstGUI()

XPT tk.lab "label for Tkinter
`lab_var^ = Label(self.root, text="`label text^")
`lab_var^.pack()


XPT tk.but "button for Tkinter
`but_var^ = Button(self.root, text="`label text^", side = `LEFT/RIGHT/TOP/BOTTOM^,
            expand=`YES/NO^, fill = `X/Y/BOTH/NONE^, anchor = `CENTER(default)/NE/SE...^)
`but_var^.grid(row=`0/1..^, sticky=`N/E/NE... ew^)
`but_var^.pack()

XPT tk.grid "The grid spec for widgets Tkinter
`var^.grid(row=`row^, column=`col^, sticky=`N/E..^, padx = `xval^, pady = `yval^)
