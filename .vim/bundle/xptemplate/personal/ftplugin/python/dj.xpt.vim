XPTemplate priority=personal


XPT dj.form.form "what goes in form
from django import forms

class MyForm(forms.Form):
    name = forms.CharField(max_length=30)

XPT dj.form.view "what goes in view to get form

from myapp.forms import MyForm
def my_view(request):
    form = MyForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        name = form.cleaned_data['name']
        #do something with the data
    return render(request, 'myapp/myform.html', {'form':form})

XPT dj.form.template "what goes in template

<html>                                                         
<body>                                                         
<form action="{% url 'myapp:my_form' %}"                       
    method="post">                                             
    {% csrf_token %}                                           
    {{ form.as_p }}                                            
    <button type="submit">Go!</button>                         
</form>                                                        
</body>                                                        
</html>  
