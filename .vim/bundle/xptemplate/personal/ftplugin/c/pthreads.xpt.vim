XPTemplate priority=personal

XPT .pth.init "a simple pthreads skeleton
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>

void `target_func^(void * a){
    printf("a is %d\n",(int)*a);
}

int main(int argc, char **arg) {
    pthread_t thread1;
    int r1 = 42;
    if (pthread_create(&thread1, NULL, `target_func^,(void *) &r1) != 0){
        perror("pthread_create");
        exit(1); 
    }

    if (pthread_join(thread1, NULL) != 0)
        perror("pthread_join"), exit(1);

    return 0;
}

XPT .pth.mutex "create a mutex block

pthread_mutex_t `mutex_nam^=PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_lock(&`mutex_nam^);
pthread_mutex_unlock(&`mutex_nam^);


