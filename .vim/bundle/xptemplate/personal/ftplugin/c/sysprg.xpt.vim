XPTemplate priority=personal


XPT fork.h "headers for forking
#include <unistd.h>

XPT fork "basic fork
if ((pid = (fork()) == 0) {
    `CURSOR^
    exit(0);
}

XPT forkn "fork n children
for (x = 0; x < nchildren; x++) {
    if ((pid = fork()) == 0) {
        child_func(x + 1);
        exit(0);
    }
}

XPT ipc.soc.h "ipc socket headers, port defines
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>

#define SERVERPORT 8888

XPT ipc.soc.serv "simple server template
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#define SERVERPORT 8888

int main()
{
    int socket1,socket2;
    int addrlen;
    struct sockaddr_in xferServer, xferClient;
    /* create a socket */
    socket1 = socket(AF_INET, SOCK_STREAM, 0);
    if (socket1 == -1)
    {
        fprintf(stderr, "Could not create socket!\n");
        exit(1);
    }
    /* bind to a socket, use INADDR_ANY for all local addresses */
    xferServer.sin_family = AF_INET;
    xferServer.sin_addr.s_addr = INADDR_ANY;
    xferServer.sin_port = htons(SERVERPORT);
    returnStatus = bind(socket1,
            (struct sockaddr*)&xferServer,
            sizeof(xferServer));
    if (returnStatus == -1)
    {
        fprintf(stderr, "Could not bind to socket!\n");
        exit(1);
    }
    returnStatus = listen(socket1, 5);
    if (returnStatus == -1)
    {
        fprintf(stderr, "Could not listen on socket!\n");
        exit(1);
    }
    for(;;)
    {
        addrlen = sizeof(xferClient);
        /* use accept() to handle incoming connection requests */
        /* and free up the original socket for other requests */
        socket2 = accept(socket1, (struct sockaddr*)&xferClient, &addrlen);
        if (socket2 == -1)
        {
            fprintf(stderr, "Could not accept connection!\n");
            exit(1);
        }
        /******* Heart of the Server */
        // fork a child worker
        // don't forget to close(socket2)
    }
    close(socket1);
    return 0;
}

XPT ipc.soc.client "template client
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#define SERVERPORT 8888
#define MAXBUF 1024

//argv[1] is the ip address of server
int main(int argc, char* argv[])
{
    int sockd;
    int counter;
    int fd;
    struct sockaddr_in xferServer;
    int returnStatus;
    char buf[MAXBUF];
    /* create a socket */
    sockd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockd == -1)
    {
        fprintf(stderr, "Could not create socket!\n");
        exit(1);
    }
    /* set up the server information */
    xferServer.sin_family = AF_INET;
    xferServer.sin_addr.s_addr = inet_addr(argv[1]);
    xferServer.sin_port = htons(SERVERPORT);
    /* connect to the server */
    returnStatus = connect(sockd,
            (struct sockaddr*)&xferServer,
            sizeof(xferServer));
    if (returnStatus == -1)
    {
        fprintf(stderr, "Could not connect to server!\n");
        exit(1);
    }
    returnStatus = write(sockd, argv[2], strlen(argv[2])+1);
    /* call shutdown to set our socket to read-only */
    shutdown(sockd, SHUT_WR); //if you want it to be readonly
    while ((counter = read(sockd, buf, MAXBUF)) > 0)
    {
        /* Heart of client read and write back */
    }
    close(sockd);
    return 0;
}
