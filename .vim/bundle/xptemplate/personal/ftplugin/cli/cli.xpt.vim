XPTemplate priority=personal

XPT xpt " Main Categories
`g. git related
gh.cs git help and case studies
k. key related
w. windows cli stuff^

XPT g.i "int, add and commit changes
git init `--bare #to create codeless repos^
`touch proj.git/git-daemon-export-ok #incase you need ssh^
git add .
git commit -m `1:commit message^

XPT g.commit "commit -a -m
git commit -a -m `1:commit message^

XPT g.rm "remove dir
git rm ` -r^ `2: directory/list of file^

XPT g.diff "diff stuffs
git diff master..`yourbranc^
git diff HEAD -- `.\lib^ #diffing to particular folder alone
git diff --stat

XPT g.l
git `log or reflog^
git log --pretty=format:"%h %an %ar : %s"
git log --pretty=format:"%h : %s" --topo-order --graph #very cool thing!
git log --pretty=format:"%h : %s" --date-order --graph #very cool thing!

XPT g.push
git push `ssh://yourserver.com/~you/proj.git^ master
git push public-repo master `don't forget to update .git/config remote^

XPT g.tag
git tag -s `your tag label^ `hashcode^ #if you had config for keys
git tag -u `gpg-key-id^ `label^ `hashcode^ #no config needed

XPT g.grep
git grep -n -e '#define' --and -e SORT_DIRENT


XPT g.reset "Reset commits
git reset --hard  `1:hashofcommit^

XPT g.revert "Revert commits
git revert `1:hashofcommit^

XPT g.co "Checkout branchs
git checkout `-b branch to create branch^
git checkout `HEAD or --^ `filename.txt^  `#HEAD for cache, -- for current version?^

XPT gh.undo "some undo commands
g.res #git reset
g.rev #git revert
g.filter

XPT g.m "merge
git merge `1:merge in branch to here^

XPT g.ammend "ammend
git commit --ammend `1: -a #add few more commits to last commit^

XPT g.rebase "reabse sequence
git rebase -i HEAD~`1:count^  `--onto toavoid interaction^ `2:#pick,reword,edit, squash, fixup?^
git rebase --continue
git rebase --abort

XPT g.filter "delete secret files from repo
git filter-branch --tree-filter 'rm `1:creditcard file path^'


XPT gh.cs.fastimp "fast import repo from hand made history files
git fast-import --date-format=rfc2822 < /tmp/history


XPT gh.cs.bisect  "bisect to catch the code break with automation
git bisect start
git bisect bad HEAD
git bisect good `hashcode^
git bisect reset #to stop and return to old state
git bisect run `myscript^ #runs automation script

XPT gh.cs.config  "Configure global name, email, alias, remoteurl
git config --global user.name `yourname^
git config --global user.email `email^
git config --global alias.co checkout  #git co is no git checkout
git config remote.origin.url `git://newurl/proj.git^
git config --get remote.origin.url

XPT gh.cs.remote  "remote add, pull, push
git remote add `remotehandle^ `git url^
git pull `remotehandle^ `branchname^

XPT gh.cs.resethead "resetting head and using orig_head to recover
git reset HEAD~3 #roll backs the time and head is now 3 commits before
git reset ORIG_HEAD #back to normal! ORIG_HEAD is back pointer
git reset `your commit hash^

XPT gh.cs.bold "del force clean, untracked, abandon uncommited changes
git reset -f HEAD\^
git reset --hard `hash^
git branch -D `branch with uncommitted changes^ #-d is for normal kids
git clean -f -d #cleans up all untracked files mercilessly!

XPT gh.cs.hosting "setting up public repos and private repos
$ git --bare update-server-info
$ chmod a+x hooks/post-update #on a private http server
now clients can do git clone http://...
for scp a bare bones is uploaded to server
$ git clone --bare /home/user/myrepo/.git /tmp/myrepo.git
$ scp -r /tmp/myrepo.git myserver.com:/opt/git/myrepo.git
now clients can git clone myserver.com:/opt...


XPT gh.notes "some notes"
pull = fetch + merge
tags can be used as commithash code


""""""""""""""" GPG key clis"
XPT k.ssl "openSSL way to gen private and pub keys
openssl genrsa -aes128 -out fd.key 2048
openssl rsa -in fdpub.key -pubout -out fdpub2.key
openssl ecparam -out fd.key -name prime256v1 -genkey #not working in windows

Display certificate information

XPT k.revoke "revoking the key
gpg -ao ~/myrevoke.asc --gen-revoke your_user_id
gpg --import ~/myrevoke.asc
gpg --keyserver pgp.mit.edu --send-keys your_user_id
#method 2
1. gpg --allow-secret-key-import --import mykeys.asc
2. gpg --list-keys
3. gpg --edit-key E7E32C2A
4. gpg> revsig
5. gpg>save or quit
6. gpg --keyserver pgp.mit.edu --send-key E7E32C2A

XPT k.gen "Gen, list, del keys
gpg --gen-key
gpg --list-secret-keys
gpg --list-keys
gpg --delete-secret-key "username"
gpg --delete-key "username"

XPT k.impexp "export import keys to key ring
gpg -a --export-secret-keys XXXXXXXXX | gpg -aco privatekey.pgp.asc
gpg -ao publickey.asc --export name@domain.com
a = ascii armor, c = crypt, o = output to file
gpg --allow-secret-key-import --import private.key
gpg --import public.key

XPT k.encdec "basic encryption and decrption
gpg -e -u "yourname" -r "receivername" yourfile.zip
gpg -d yourfile.zip.gpg

XPT k.server "send, search and receive keys from a keyserver
gpg --keyserver  pgp.mit.edu --send-keys XXXXXXXX
gpg --keyserver  pgp.mit.edu --recv-key XXXXXXXX
gpg --search-keys "Gopinath Sadasivam"

XPT k.signverify "sign text and verify
gpg -as del.txt
gpg --clearsign del.txt #(produces del.txt.asc)
gpg -d `clearsigned text file^

XPT w.wifi
netsh wlan set hostednetwork mode=allow ssid=hotblue key=home1305536
netsh wlan start hostednetwork #often this is enough
