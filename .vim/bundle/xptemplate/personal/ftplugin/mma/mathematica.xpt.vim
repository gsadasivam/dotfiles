XPTemplate priority=personal

XPT sol "Solve simultaenous equations
Solve[{`x+y^ == 0, `x-y^ == 0}, {`x^, `y^}]
Solve[`x+x2^ == 0, x]

XPT exp "Expand, Factor and Simlplify
Expand[`arg^]
Factor[`arg^]
Simplify[`arg^]

"  =====
XPT perm "Permutations
Permutations[{a, b, c, d}, {3}]
Permutations[{a, b, c, d}]

XPT r.c "Random Choice or sampling
RandomChoice[{0.2, 0.2, 0.6} -> {a, b, c}, 20] *a,b,c with prob 20-20-60% and 20 times*
RandomSample[Range[30], 30] #non repeating random permutation


XPT p "Plot 2d 3d stuffs
Plot[ {`Sin[x]^, `Cos[x]^}, {x,0,Pi},
        PlotRange -> { `xstart^, `xend^}, AxesOrigin -> {`x_orig^, `y_orig^}];
`cursor^

XPT p.pie "Pie Chart 3D optional
PieChart`3D^[ {`2,3,4,5^},
    ChartLabels -> {`"a","b","c","d"^}, ChartStyle -> "Pastel", PlotLabel -> "`Title^"]

XPT p.l "List Plot
ListPlot[`List of something^]

XPT p.para "Plot 2d 3d stuffs
ParametricPlot[{`Sin[t]^, `Cos[t]^}, {t,0,Pi},
        PlotStyle -> RGBColor[1,0,1],
        AspectRatio -> 1.2];

XPT p.3d "3d plot with view point
Plot3D[`Sqr[x\^2+y\^2\]^ , {x,-1,1}, {y, -1, 1}, Mesh -> False
        BoxRatios -> {1,1,1}, ViewPoint -> {-1.2, -2, 0}, PlotPoints -> 30];

XPT p.c "Contour Plot
ContourPlot[`f(x,y)^, {x, `xstart^, `xend^}, {y, `ystart^, `yend^},
PlotPoints -> 10, Contours -> 15, ContourLines -> True,
ContourShading -> False];

XPT r "a 20x3 random real between two float/int values
Table[Random[`Real/Integer^, {-2.3, 3.4}], {i, 1, 20}, {j, 1, 3}] // TableForm
RandomReal[ {0,1}, {`m^, `n^}]


XPT man "Manipulate a plot
Manipulate [
    Plot [
        `fx with n_var^, {`x,start,end^}
    ],
    {`n_var^,`start^, `stop^}
]

XPT man.grid "Two or more charts in manipulate window
Manipulate[
    GraphicsGrid [
        {
            {
                Plot[`blah blah^],
                LogPlot[`blah blah^]
            }
        }
    ],
    {`n_var^,`start^, `stop^, Appearance -> "Labelled"}
]

XPT ani "Animate better than Manipulate
Animate [
    Plot [
        `fx with n_var^, {`x,start,end^}
    ],
    {`n_var^,`start^, `stop^}
]
