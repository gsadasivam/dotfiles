#include <iostream>
using namespace std;

class MyClass {
public:
    MyClass();
    ~MyClass();
    MyClass( const MyClass &cpy );

private:
};

MyClass::MyClass() {
    cout << "MyClass constructor called\n";
}

MyClass::~MyClass() {
    cout << "MyClass destructor called.\n";
}

MyClass::MyClass( const MyClass &cpy ) {
    cout << "MyClass copy constructor called\n";
}

int main(int argc, char *argv[])
{
    cout <<"Welcome!\n";
    MyClass * ptr_MyClass = new MyClass();
    delete ptr_MyClass;
    cout <<"Exiting...\n";
    return 0;
}
