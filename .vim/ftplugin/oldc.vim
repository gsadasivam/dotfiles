map <F5> <ESC>:w<CR>:!gcc % && ./a.out<CR>
map! <F5> <ESC>:w<CR>:!gcc % && ./a.out<CR>

"remove trailing spaces
au BufWritePre * :%s/\s\+$//e
au BufWritePre *.c :%!clang-format
au BufWritePre *.cpp :%!clang-format

