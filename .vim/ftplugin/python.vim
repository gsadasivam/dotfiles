map <F5> <ESC>:w<CR>:!python % <CR>
map! <F5> <ESC>:w<CR>:!python % <CR>

au BufWritePre * :%s/\s\+$//e

let g:autopep8_disable_show_diff=1

map <C-k> :w<CR>:%!autopep8 %<CR>:w<CR>
imap <C-k> <ESC>:w<CR>:%!autopep8 %<CR>:w<CR>
