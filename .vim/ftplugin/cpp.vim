map <F5> <ESC>:w<CR>:!g++ % && ./a.out<CR>
map! <F5> <ESC>:w<CR>:!g++ % && ./a.out<CR>

let g:syntastic_cpp_compiler = 'g++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

"remove trailing spaces
au BufWritePre * :%s/\s\+$//e
au BufWritePre *.c :%!clang-format
au BufWritePre *.cpp :%!clang-format

