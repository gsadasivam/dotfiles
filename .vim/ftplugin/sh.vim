map <F5> <ESC>:w<CR>:!bash %<CR>
map! <F5> <ESC>:w<CR>:!bash %<CR>

"remove trailing spaces
au BufWritePre * :%s/\s\+$//e
