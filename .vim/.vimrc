
"GUI SETTINGS
    "set guifont=Consolas:h10.07:w5.62:b
    "set guifont=Menlo:h11:w5.8:b
    set go-=M                   "All gui menus can goto hell!"
    set go-=m                   "Takes away menu, Tool bars, righ-left scrollbars
    set go-=T
    set go-=r			"Preserve the right scrollbar in linux.
    set go-=l
    set go-=R
    set go-=L
    set go+=a                   "selected texts are ready to copy paste

    " disable mouse
    set mouse=

"Editor Variables nu,ai,tabs,menus,autoread,ssop etc.
    set nu
    set ai
    set backspace=2
    set nocp
"    set clipboard=unnamed           " Directly use windows clipboard instead of internal buffer
    set ff=dos
    "set cursorline
    set ignorecase                  " case insensitive search
    set smartcase                   " case sensitive when uc present
    set wildmenu                    " show list instead of just completing
    set wildmode=list:longest,full  " command <Tab> completion, list matches, then longest common part, then all.
"    set lines=40                " 40 lines of text instead of 24,
    set expandtab                   " Tabs into spaces
    set shiftwidth=4
    set softtabstop=4
    set autoread                    " Detect the file changes made outside vim and update

    set ssop-=options    " do not store global and local values in a session
    set ssop-=folds      " do not store folds

    set noswapfile      " who needs swap files turn it off!
    set nobackup
    set nowb
    "set lazyredraw                  "don't redraw during macro executions
    set tw=60           " yes i like narrow columns!

set term=xterm          " needed for proper tmux functioning.

" vertical split beautification
" hi VertSplit guibg=black guifg=black
" hi VertSplit ctermbg=black ctermfg=black
" set fillchars=vert:║,fold:-

" save with root permission!
cnoremap w!! w !sudo tee > /dev/null %

filetype plugin on

syn on
au BufRead *.nlogo set filetype=nlogo
au BufNewFile *.nlogo set filetype=nlogo
au BufRead bash-f* set filetype=cli


" syntax highlight fucks up entire vim, so no please for this crazy file
au BufRead *.xpt.vim syn off
au BufNewFile *.xpt.vim syn off
au BufNewFile *.xpt.vim 0r $HOME/.vim/skel/xpt_skel.vim
au BufNewFile *.py 0r $HOME/.vim/skel/python_skel.py | let IndentStyle = "py"
au BufNewFile *.sh 0r $HOME/.vim/skel/sh.vim

" won't work when following is placed in ~/.vim/ftplugin/c.vim
au BufNewFile *.c 0r $HOME/.vim/skel/c_skel.vim
au BufNewFile *.cpp 0r $HOME/.vim/skel/cpp_skel.vim

"  Copy pasta life saver! (X clipboard =*, vim
"  clipboard=none, system clipboard =+)
map! <S-Insert>  <ESC>"+pa
map <C-Insert>  "+y
map <F2> :w<CR>
map! <F2> <ESC>:w<CR>a
map <F12> :wq<CR>
map! <F12> <ESC>:wq<CR>

"Plugin Configs
execute pathogen#infect('~/.vim/bundle/{}')
call pathogen#helptags()
" colorscheme ron

set guifont=Droid\ Sans\ Mono\ for\ Powerline\ 10
set encoding=utf-8

set foldmethod=indent
nnoremap <space> za
vnoremap <space> zf
vnoremap <2-LeftMouse> za
vnoremap <2-LeftMouse> zf

let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    \ 'file': '\v\.(exe|so|dll|pyc)$',
    \ }

let g:ctrlspace_symbols = {
        \ "cs"      : "#",
        \ "tab"     : "TAB",
        \ "all"     : "ALL",
        \ "open"    : "OPEN",
        \ "tabs"    : "-",
        \ "c_tab"   : "+",
        \ "load"    : "LOAD",
        \ "save"    : "SAVE",
        \ "prv"     : "*",
        \ "s_left"  : "[",
        \ "s_right" : "]"
        \ }

hi Normal guibg=Black

"Session Management
nnoremap <leader>so :OpenSession
nnoremap <leader>ss :SaveSession
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>


let g:session_autosave = 'no'
let g:session_autoload = 'no'

" for silver searcher Ag
let g:ag_prg="ag --vimgrep --smart-case"

"airline config
        let g:airline_section_z = ''
        let g:airline#extensions#syntastic#enabled = 1

        let g:airline_left_sep=''
        let g:airline_right_sep=''
        let g:airline_theme='badwolf'
        let g:airline_powerline_fonts=1
        let g:airline_exclude_preview = 1
        let g:airline_mode_map = {
            \ '__' : '-',
            \ 'n'  : 'N',
            \ 'i'  : 'I',
            \ 'R'  : 'R',
            \ 'c'  : 'C',
            \ 'v'  : 'V',
            \ 'V'  : 'V',
            \ '' : 'V',
            \ 's'  : 'S',
            \ 'S'  : 'S',
            \ '' : 'S',
            \ }
        set laststatus=2



" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
    let g:UltiSnipsEditSplit="vertical"


"XPTemplate
    let g:xptemplate_vars = "BRfun= "  "handles function code format

autocmd FileType javascript,css,c,cpp,java nnoremap <silent> <Leader>; :call cosco#commaOrSemiColon()<CR>
autocmd FileType javascript,css,c,cpp,java inoremap <silent> <Leader>; <c-o>:call cosco#commaOrSemiColon()<CR>


" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = 'opt/anaconda2/bin/python'


"" startify
"let g:startify_bookmarks = 1
"let g:startify_change_to_dir = 1
"let g:startify_change_to_vcs_root = 1
"let g:startify_custom_header = 1
"let g:startify_enable_special = 1
"let g:startify_list_order = 1
"let g:startify_relative_path = 1
"let g:startify_skiplist = 1

set colorcolumn=60
highlight colorcolumn ctermfg=red ctermbg=None
highlight Folded guibg=black guifg=white ctermfg=white ctermbg=black
